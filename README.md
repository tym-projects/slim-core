[![pipeline status](https://gitlab.com/tym-projects/slim-core/badges/master/pipeline.svg)](https://gitlab.com/tym-projects/slim-core/-/commits/master)
[![coverage report](https://gitlab.com/tym-projects/slim-core/badges/master/coverage.svg)](https://gitlab.com/tym-projects/slim-core/-/commits/master)
[![Latest Release](https://gitlab.com/tym-projects/slim-core/-/badges/release.svg)](https://gitlab.com/tym-projects/slim-core/-/releases)
# Slim Core for Slim Projects

