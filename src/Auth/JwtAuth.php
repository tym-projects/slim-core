<?php declare (strict_types = 1);

namespace TYM\SlimCore\Auth;

use DomainException;
use Exception;
use Firebase\JWT\JWT;
use TYM\SlimCore\Auth\JwtToken;

class JwtAuth
{

    /**
     * @param null|string $secret
     * @param int $lifetime
     * @return void
     */
    public function __construct(
        private  ? string $secret,
        private int $lifetime
    ) {
    }

    /**
     * @param string $value
     * @param int|null $lifetime
     * @return JwtToken
     * @throws DomainException
     */
    public function generateToken(string $value, ?string $secret = null, ?int $lifetime = null) : JwtToken
    {
        $time = time();

        $payload = [
            'iat' => $time,
            'exp' => $time + (($lifetime) ?? $this->lifetime),
            'value' => $value,
        ];

        return new JwtToken
            (JWT::encode($payload,
            ($secret) ?? $this->secret)
        );
    }

    /**
     * @param JwtToken $jwttoken
     * @param string|null $secret
     * @return bool
     */
    public function validateToken(JwtToken $jwttoken, ?string $secret = null): bool
    {
        try {
            $decode = get_object_vars(
                JWT::decode(
                    $jwttoken->getToken(),
                    (($secret) ?? $this->secret),
                    ['HS256']
                )
            );
            $this->value = $decode['value'];
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
