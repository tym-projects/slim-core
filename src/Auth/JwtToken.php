<?php declare (strict_types = 1);

namespace TYM\SlimCore\Auth;

class JwtToken
{

    /**
     * @param string $token
     * @return void
     */
    public function __construct(
        private string $token
    ) {
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
