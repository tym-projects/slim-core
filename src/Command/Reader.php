<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Command;

final class Reader
{

    /**
     * @return mixed
     */
    public static function read(): mixed
    {
        fscanf(
            STDIN,
            "%[a-zA-Z0-9.@- ]\n",
            $value
        );
        return ($value) ? $value : null;
    }
}
