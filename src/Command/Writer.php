<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Command;

final class Writer
{

    /**
     *
     * @param string $message
     * @param mixed $values
     * @return void
     */
    public static function write(
        string $message,
        ...$values
    ) {
        fwrite(
            STDOUT,
            sprintf(
                $message,
                ...$values
            )
        );
    }
}
