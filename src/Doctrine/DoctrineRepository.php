<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use TYM\SlimCore\Doctrine\EntityInterface;

abstract class DoctrineRepository
{
    public function __construct(
        private EntityManager $entityManager
    ) {
    }

    protected function entityManager(): EntityManager
    {
        return $this->entityManager;
    }

    protected function persist(EntityInterface $entity): void
    {
        $this->entityManager()->persist($entity);
        $this->entityManager()->flush($entity);
    }

    protected function remove(EntityInterface $entity): void
    {
        $this->entityManager()->remove($entity);
        $this->entityManager()->flush($entity);
    }

    protected function repository(string $entityClass): EntityRepository
    {
        return $this->entityManager->getRepository($entityClass);
    }
}
