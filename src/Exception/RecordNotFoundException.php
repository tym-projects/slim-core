<?php
declare (strict_types = 1);

namespace TYM\SlimCore\Exception;

use Exception;

/**
 * @package TYM\Core\Exception
 */
class RecordNotFoundException extends Exception
{
}
