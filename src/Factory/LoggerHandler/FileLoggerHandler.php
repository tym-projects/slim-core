<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Factory\LoggerHandler;

use InvalidArgumentException;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * @testFunction testFileLoggerHandler
 */
final class FileLoggerHandler
{

    /**
     * $settings = [
     *   'path' => Folder in which the log files will be stored
     *   'filename' => The logs file name
     *   'level' => Optional logger level (default Logger::DEBUG)
     *   'permission' => Optional file permissions (default (0644) are only for owner read/write)
     * ]
     *
     * @param array $settings
     * @return RotatingFileHandler
     */
    public static function createFrom(array $settings): RotatingFileHandler
    {
        if (!isset($settings['path'])) {
            throw new InvalidArgumentException(sprintf("path not exists in settings"));
        }

        if (!isset($settings['filename'])) {
            throw new InvalidArgumentException(sprintf("'filename' not exists in settings"));
        }

        return (
            new RotatingFileHandler(
                sprintf("%s/%s", $settings['path'], $settings['filename']),
                0,
                $settings['level'] ?? Logger::DEBUG,
                true,
                $settings['permission'] ?? null
            ))->setFormatter(
            new LineFormatter(null, null, false, true)
        );
    }
}
