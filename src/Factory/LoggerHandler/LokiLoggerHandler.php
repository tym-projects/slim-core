<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Factory\LoggerHandler;

use Itspire\MonologLoki\Handler\LokiHandler;
use Monolog\Logger;

/**
 * @testFunction testLokiLoggerHandler
 */
final class LokiLoggerHandler
{

    /**
     * $settings = [
     *  'entrypoint' => URL entrypoint loki api
     *  'context' => [] Array on set your globally applicable context variables
     *  'labels' => [] Array with on set your globally applicable labels
     *  'client_name' => Unique identifier for the client host
     *  'auth' => [] Optional array authentication ej: ['basic' => ['user', 'password']]
     *  'curl_options' => [] Optional array override the default curl options with custom values
     *  'level' => Optional logger level (default Logger::DEBUG)
     * ]
     *
     * @param array $settings
     * @return StreamHandler
     */
    public static function createFrom(array $settings): LokiHandler
    {
        return new LokiHandler(
            $settings,
            $settings['level'] ?? Logger::DEBUG
        );
    }
}
