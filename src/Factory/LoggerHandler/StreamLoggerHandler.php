<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Factory\LoggerHandler;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * @testFunction testStreamLoggerHandler
 */
final class StreamLoggerHandler
{

    /**
     *$settings = [
     *  'level' => Optional logger level (default Logger::DEBUG)
     * ]
     *
     * @param array $settings
     * @return StreamHandler
     */
    public static function createFrom(array $settings): StreamHandler
    {
        return (
            new StreamHandler(
                'php://stdout', $settings['level'] ?? Logger::DEBUG
            ))->setFormatter(
            new LineFormatter(null, null, false, true)
        );
    }
}
