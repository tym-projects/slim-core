<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Factory;

use InvalidArgumentException;
use TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler;
use TYM\SlimCore\Factory\LoggerHandler\LokiLoggerHandler;
use TYM\SlimCore\Factory\LoggerHandler\StreamLoggerHandler;

/**
 * @testFunction testBuildHandlers
 */
final class LoggerHandlerFactory
{
    const LOGGER_HANDLERS = [
        'filehandler' => FileLoggerHandler::class,
        'streamhandler' => StreamLoggerHandler::class,
        'lokihandler' => LokiLoggerHandler::class,
    ];

    /**
     * @param array $settings
     * @return array
     * @throws InvalidArgumentException
     */
    public static function buildHandlersFrom(array $settings = []): array
    {
        $handlers = [];

        foreach ($settings as $key => $setting) {
            if (!array_key_exists($key, self::LOGGER_HANDLERS)) {
                throw new InvalidArgumentException(
                    sprintf("%s no implement the %s logger handler", LoggerHandlerFactory::class, $key)
                );
            }
            $handlers[] = self::LOGGER_HANDLERS[$key]::createFrom($setting);
        }

        return $handlers;
    }
}
