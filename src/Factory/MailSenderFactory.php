<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Factory;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * @testFunction testMailSenderFactory
 */
class MailSenderFactory
{
    /**
     * @param PHPMailer $phpmailer
     * @return void
     */
    public function __construct(
        private PHPMailer $phpmailer
    ) {
    }

    /**
     * @param array $settings = [
     *  "SMTP_PORT" => "25"
     *  "SMTP_HOST" => "mail.example.com"
     *  "SMTP_USER" => "user"
     *  "SMTP_PASS" => "password"
     *  "FROM_EMAIL" => "info@example.com"
     *  "FROM_NAME" => "Example User"
     * ]
     *
     * @return MailSenderFactory
     */
    public function configSender(array $settings): MailSenderFactory
    {
        $this->phpmailer->IsSMTP();
        $this->phpmailer->Port = $settings['SMTP_PORT'];
        $this->phpmailer->Host = $settings['SMTP_HOST'];
        $this->phpmailer->Username = $settings['SMTP_USER'];
        $this->phpmailer->Password = $settings['SMTP_PASS'];
        $this->phpmailer->IsHTML(true);
        $this->phpmailer->SMTPSecure = "tls";
        $this->phpmailer->CharSet = 'UTF-8';
        $this->phpmailer->SMTPAuth = true;
        $this->phpmailer->From = $settings['FROM_EMAIL'];
        $this->phpmailer->FromName = $settings['FROM_NAME'];

        return $this;
    }

    /**
     * @param array $destination
     * @param string $subject
     * @param string $body
     * @param array $attachments
     * @return bool
     * @throws Exception
     */
    public function send(
        array $destination,
        string $subject,
        string $body,
        array $attachments = []
    ): bool {
        $this->phpmailer->Body = $body;

        foreach ($attachments as $value) {
            $this->phpmailer->addAttachment($value);
        }

        $this->phpmailer->Subject = $subject;
        $this->phpmailer->AltBody = $subject;
        if (sizeof($destination) > 1) {
            foreach ($destination as $value) {
                $this->phpmailer->addBCC($value);
            }
        } else {
            $this->phpmailer->addAddress($destination[0]);
        }

        return $this->phpmailer->Send();
    }
}
