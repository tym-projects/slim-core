<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Middelware;

use Exception;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use TYM\SlimCore\Auth\JwtAuth;
use TYM\SlimCore\Auth\JwtToken;

final class AuthorizationMidderware implements MiddlewareInterface
{
    const REGEX = "/Bearer\s+(.*)$/i";
    const HEADER = "Authorization";

    /**
     * @param ResponseFactoryInterface $responseFactory
     * @param JwtAuth $jwtAuth
     * @param LoggerInterface $logger
     * @return void
     */
    public function __construct(
        private ResponseFactoryInterface $responseFactory,
        private JwtAuth $jwtAuth,
        private LoggerInterface $logger
    ) {
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        try {

            $token = null;

            $header = $request->getHeaderLine(self::HEADER);
            if (false === empty($header)) {
                if (preg_match(self::REGEX, $header, $matches)) {
                    $token = $matches[1];
                }
            }

            $token = ($token) ? $token : $request->getQueryParams()['token'];

            if (!$token) {
                throw new RuntimeException(
                    sprintf("Token not found in access point %s", $request->getUri())
                );
            }

            if (!$this->jwtAuth->validateToken(
                new JwtToken($token),
                $request->getAttribute('config')['secret'])
            ) {
                throw new Exception(
                    sprintf("Token '%s' Not Valid", $token)
                );
            };

            return $handler->handle($request);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $response = $this->responseFactory->createResponse(401);
            return $response;
        }
    }

}
