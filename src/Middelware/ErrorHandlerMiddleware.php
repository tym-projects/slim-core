<?php

namespace TYM\SlimCore\Middelware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

final class ErrorHandlerMiddleware implements MiddlewareInterface
{

    /**
     * @param LoggerInterface $logger
     * @return void
     */
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $errorTypes = E_ALL;

        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                switch ($errno) {
                    case E_USER_ERROR:
                        $this->logger->error(
                            "ERROR $errstr on line $errline in file $errfile"
                        );
                        break;
                    case E_USER_WARNING:
                        $this->logger->warning(
                            "WARNING $errstr on line $errline in file $errfile"
                        );
                        break;
                    default:
                        $this->logger->notice(
                            "NOTICE $errstr on line $errline in file $errfile"
                        );
                        break;
                }
                return true;
            },
            $errorTypes
        );

        return $handler->handle($request);
    }
}
