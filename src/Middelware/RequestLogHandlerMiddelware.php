<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Middelware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Throwable;

final class RequestLogHandlerMiddelware implements MiddlewareInterface
{

    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        $response = $handler->handle($request);

        $content = "{$request->getMethod()} {$request->getRequestTarget()} HTTP/{$request->getProtocolVersion()} {$request->getHeaders()['User-Agent'][0]} {$request->getHeaders()['Host'][0]}";

        switch ((int) $response->getStatusCode()) {
            default:
                $this->logger->info($content);
        }

        return $response;

    }
}
