<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared;

use InvalidArgumentException;

/**
 * @testFunction testAssert
 */
final class Assert
{
    /**
     * @param string $class
     * @param array $items
     * @return void
     * @throws InvalidArgumentException
     */
    public static function arrayOf(string $class, array $items): void
    {
        foreach ($items as $item) {
            self:: instanceof ($class, $item);
        }
    }

    /**
     * @param string $class
     * @param mixed $item
     * @return void
     * @throws InvalidArgumentException
     */
    public static function instanceof (string $class, $item): void {
        if (!$item instanceof $class) {
            throw new InvalidArgumentException(
                sprintf('The object <%s> is not an instance of <%s>', $class, get_class($item))
            );
        }
    }
}
