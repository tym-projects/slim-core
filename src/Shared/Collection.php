<?php declare (strict_types = 1);

namespace TYM\SlimCore\Shared;

use ArrayIterator;
use Countable;
use Exception;
use InvalidArgumentException;
use IteratorAggregate;

/**
 * @testFunction testCollection
 */
abstract class Collection implements Countable, IteratorAggregate
{

    /**
     * @var array
     */
    protected $items;

    /**
     * @param array $items
     * @return void
     * @throws InvalidArgumentException
     */
    public function __construct(array $items)
    {
        Assert::arrayOf(static::type(), $items);
        $this->items = $items;
    }

    /**
     * get item class type
     * @return string
     */
    abstract protected static function type(): string;

    /**
     * get item by array key
     * @param $key
     * @return null|mixed
     */
    abstract public function get($key);

    /**
     * get num of items in Entity
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * get items
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    /**
     * @param bool $preserve_keys
     * @return array
     * @throws Exception
     */
    public function toArray(bool $preserve_keys = true): array
    {
        $array = array();
        foreach ($this->getIterator() as $key => $value) {
            if (!method_exists($value, 'toArray')) {
                throw new Exception(
                    sprintf("%s class must be implemet toArray method", $value::class)
                );
            }

            if ($preserve_keys) {
                $array[$key] = $value->toArray();
            } else {
                $array[] = $value->toArray();
            }
        }
        return $array;
    }
}
