<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared\ValueObject;

use function Lambdish\Phunctional\reindex;
use ReflectionClass;
use Stringable;

abstract class Enum implements Stringable
{
    protected static array $cache = [];

    /**
     * @param mixed $value
     * @return void
     */
    public function __construct(protected $value)
    {
        $this->ensureIsBetweenAcceptedValues($value);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    abstract protected function throwExceptionForInvalidValue($value);

    /**
     * @param string $name
     * @param mixed $args
     * @return static
     */
    public static function __callStatic(string $name, $args)
    {
        return new static(self::values()[$name]);
    }

    /**
     * @param string $value
     * @return Enum
     */
    public static function fromString(string $value): Enum
    {
        return new static($value);
    }

    /**
     * @return array
     */
    public static function values(): array
    {
        $class = static::class;

        if (!isset(self::$cache[$class])) {
            $reflected = new ReflectionClass($class);
            self::$cache[$class] = reindex(self::keysFormatter(), $reflected->getConstants());
        }

        return self::$cache[$class];
    }

    /**
     * @return mixed
     */
    public static function randomValue()
    {
        return self::values()[array_rand(self::values())];
    }

    /**
     * @return static
     */
    public static function random(): static
    {
        return new static(self::randomValue());
    }

    /**
     * @return callable
     */
    private static function keysFormatter(): callable
    {
        return static fn($unused, string $key): string => lcfirst(
            str_replace('_', '', ucwords(strtolower($key), '_'))
        );
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @param Enum $other
     * @return bool
     */
    public function equals(Enum $other): bool
    {
        return $other == $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value();
    }

    /**
     * @param mixed $value
     * @return void
     */
    private function ensureIsBetweenAcceptedValues($value): void
    {
        if (!in_array($value, static::values(), true)) {
            $this->throwExceptionForInvalidValue($value);
        }
    }
}
