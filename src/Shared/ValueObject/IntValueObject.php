<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared\ValueObject;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testIntValueObject
 */
abstract class IntValueObject implements Stringable
{

    /**
     * @var int
     */
    protected $value;

    /**
     * @param int $value
     * @return void
     */
    public function __construct(int $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    /**
     * @param int $value
     * @throws InvalidArgumentException
     */
    abstract protected static function validate(int $value);

    /**
     * @param int $value
     * @throws InvalidArgumentException
     */
    abstract public static function from(int $value): self;

    /**
     * @return int
     */
    public function value(): int
    {
        return $this->value;
    }

    /**
     * @param IntValueObject $other
     * @return bool
     */
    public function isBiggerThan(IntValueObject $other): bool
    {
        return $this->value() > $other->value();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }
}
