<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared\ValueObject;

use Stringable;

/**
 * @testFunction testStringValueObject
 */
abstract class StringValueObject implements Stringable
{

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @param string $value
     * @return void
     */
    public function __construct(string $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    /**
     * @param int $value
     * @throws InvalidArgumentException
     */
    abstract protected static function validate(string $value);

    /**
     * @param string $value
     * @return mixed
     */
    abstract public static function from(string $value): self;

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
