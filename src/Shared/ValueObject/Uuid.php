<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared\ValueObject;

use InvalidArgumentException;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Stringable;

class Uuid implements Stringable
{
    public function __construct(protected string $value)
    {
        $this->ensureIsValidUuid($value);
    }

    public static function random(): self
    {
        return new static(RamseyUuid::uuid4()->toString());
    }

    public function encode(): string
    {
        $binary = pack("h*", str_replace('-', '', $this->value()));
        $binary = base64_encode($binary);
        $binary = str_replace('/', '_', $binary);
        $binary = str_replace('=', '', $binary);
        return $binary;
    }

    public static function fromEncode(string $encoded): self
    {
        $encoded = str_replace('_', '/', $encoded) . '==';
        $encoded = base64_decode($encoded);
        $arr = unpack("h*", $encoded);
        $string = preg_replace("/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/", "$1-$2-$3-$4-$5", $arr[1]);
        return new static($string);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(Uuid $other): bool
    {
        return $this->value() === $other->value();
    }

    public function __toString(): string
    {
        return $this->value();
    }

    private function ensureIsValidUuid(string $id): void
    {
        if (!RamseyUuid::isValid($id)) {
            throw new InvalidArgumentException(sprintf('<%s> does not allow the value <%s>.', static::class, $id));
        }
    }
}
