<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Shared\ValueObject;

interface UuidGenerator
{
    public function generate(): string;
}
