<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Utils;

use InvalidArgumentException;
use RuntimeException;

final class EnvFile implements EnvironmentFile
{

    /**
     * @return void
     */
    public function __construct()
    {}

    /**
     * Load .env files, with multiple one or multiple lines with the next schema:
     * NAME=value
     *
     * @param string $path
     * @return void
     * @throws RuntimeException
     */
    public function load(string $fullPath)
    {

        if (!is_file($fullPath)) {
            throw new InvalidArgumentException(sprintf('%s file not exist', $fullPath));
        }

        if (!is_readable($fullPath)) {
            throw new RuntimeException(sprintf('%s file is not readable', $fullPath));
        }

        $lines = file($fullPath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {

            if (strpos(trim($line), '#') === 0) {
                continue;
            }

            list($name, $value) = explode('=', $line, 2);

            $name = trim($name);
            $value = trim($value);

            if (!array_key_exists($name, $_SERVER) && !array_key_exists($name, $_ENV)) {
                putenv(sprintf('%s=%s', $name, $value));
                $_ENV[$name] = $value;
                $_SERVER[$name] = $value;
            }
        }
    }

}
