<?php

namespace TYM\SlimCore\Utils;

interface EnvironmentFile
{
    public function __construct();

    public function load(string $fullpath);
}
