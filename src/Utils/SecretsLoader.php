<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Utils;

use RuntimeException;

final class SecretsLoader
{
    const VINFRA_SECRETS_PATH = "/run/secrets/";

    private $ef;
    private $path;

    /**
     * @param EnvironmentFile $ef
     * @param string $path
     * @return void
     */
    public function __construct(EnvironmentFile $ef, string $path)
    {
        $this->ef = $ef;
        $this->path = $path;
    }

    /**
     * @param null|string $path
     * @return void
     * @throws RuntimeException
     */
    public function loadSecrets()
    {
        $files = scandir($this->path);

        foreach ($files as $file) {

            if ($file === '.' || $file === '..' || !is_file($this->path . $file)) {
                continue;
            }

            $this->ef->load($this->path . $file);
        }
    }

}
