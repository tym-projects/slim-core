<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Utils;

use InvalidArgumentException;
use RuntimeException;
use TYM\SlimCore\Utils\EnvironmentFile;

final class VInfraFile implements EnvironmentFile
{

    /**
     * @var null|string
     */
    private $path;

    /**
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param string $path
     * @return void
     * @throws RuntimeException
     */
    public function load(string $filePath)
    {

        if (!is_file($filePath)) {
            throw new InvalidArgumentException(sprintf('%s file not exist', $filePath));
        }

        if (!is_readable($filePath)) {
            throw new RuntimeException(sprintf('%s file is not readable', $filePath));
        }

        $lines = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($lines as $line) {

            if (strpos(trim($line), '#') === 0) {
                continue;
            }

            $file = pathinfo($filePath);
            $name = trim($file['basename']);
            $value = trim($line);

            $values = explode(";", $value);

            if (sizeof($values) > 1) {
                foreach ($values as $v) {

                    list($name, $value) = explode('=', $v, 2);
                    $name = trim($name);
                    $value = trim($value);
                    if (!array_key_exists($name, $_SERVER) && !array_key_exists($name, $_ENV)) {
                        putenv(sprintf('%s=%s', $name, $value));
                        $_ENV[$name] = $value;
                        $_SERVER[$name] = $value;
                    }
                }
            } else {
                if (!array_key_exists($name, $_SERVER) && !array_key_exists($name, $_ENV)) {
                    putenv(sprintf('%s=%s', $name, $value));
                    $_ENV[$name] = $value;
                    $_SERVER[$name] = $value;
                }
            }

        }
    }

}
