<?php declare (strict_types = 1);

namespace TYM\SlimCore\Tests\TestClasses;

use Exception;
use TYM\SlimCore\Shared\Collection;

class FooCollection extends Collection
{

    protected static function type(): string
    {
        return Foo::class;
    }

    public function get($key): Foo
    {
        if (!array_key_exists($key, $this->items)) {
            throw new Exception('item not found');
        }
        return $this->items[$key];
    }
}
