<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Tests\TestClasses;

use TYM\SlimCore\Shared\ValueObject\IntValueObject;

final class IntBar extends IntValueObject
{

    protected static function validate(int $value)
    {}

    /**
     * @param int $value
     * @return IntBar
     */
    public static function from(int $value): self
    {
        static::validate($value);
        return new self($value);
    }

}
