<?php

declare (strict_types = 1);

namespace TYM\SlimCore\Tests\TestClasses;

use TYM\SlimCore\Shared\ValueObject\StringValueObject;

final class StringBar extends StringValueObject
{

    protected static function validate(string $value)
    {}

    /**
     * @param string $value
     * @return StringBar
     */
    public static function from(string $value): self
    {
        static::validate($value);
        return new self($value);
    }

}
