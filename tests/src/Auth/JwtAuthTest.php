<?php
namespace PHPTDD\src\Auth;

use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Auth\JwtAuth;
use TYM\SlimCore\Auth\JwtToken;

class JwtAuthTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Auth\JwtAuth
     * @covers TYM\SlimCore\Auth\JwtToken
     **/
    public function testJwtAuthInvalidTocken()
    {

        $token = new JwtToken('NO VALID TOKEN');
        $j = new JwtAuth('valid', 7200);
        $this->assertFalse($j->validateToken($token));
    }

    /**
     * @covers TYM\SlimCore\Auth\JwtAuth
     * @covers TYM\SlimCore\Auth\JwtToken
     **/
    public function testJwtAuth()
    {
        $j = new JwtAuth('secret', '100');

        $token = $j->generateToken('value');

        $this->assertTrue($j->validateToken($token, 'secret'));
        $this->assertEquals('value', $j->getValue());
    }
}
