<?php
namespace PHPTDD\src\Factory\LoggerHandler;

use InvalidArgumentException;
use Monolog\Handler\RotatingFileHandler;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler;

class FileLoggerHandlerTest extends TestCase
{

    const CONFIG = [
        'path' => __DIR__,
        'filename' => 'hello.txt',
    ];

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler
     **/
    public function testFileLoggerHandler()
    {
        $lh = FileLoggerHandler::createFrom(self::CONFIG);
        $this->assertEquals(RotatingFileHandler::class, get_class($lh));
    }

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler
     **/
    public function testFileLoggerHandlerInvalidConfigPath()
    {
        $this->expectException(InvalidArgumentException::class);
        $lh = FileLoggerHandler::createFrom([]);
    }

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler
     **/
    public function testFileLoggerHandlerInvalidConfigFilename()
    {
        $this->expectException(InvalidArgumentException::class);
        $lh = FileLoggerHandler::createFrom(['path' => './']);
    }
}
