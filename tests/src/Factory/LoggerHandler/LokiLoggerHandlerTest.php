<?php
namespace PHPTDD\src\Factory\LoggerHandler;

use Itspire\MonologLoki\Handler\LokiHandler;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Factory\LoggerHandler\LokiLoggerHandler;

class LokiLoggerHandlerTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandler\LokiLoggerHandler
     **/
    public function testLokiLoggerHandler()
    {
        $lh = LokiLoggerHandler::createFrom(['entrypoint' => 'http://loki:3100']);
        $this->assertEquals(LokiHandler::class, get_class($lh));
    }
}
