<?php
namespace PHPTDD\src\Factory\LoggerHandler;

use Monolog\Handler\StreamHandler;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Factory\LoggerHandler\StreamLoggerHandler;

class StreamLoggerHandlerTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandler\StreamLoggerHandler
     **/
    public function testStreamLoggerHandler()
    {
        $sh = StreamLoggerHandler::createFrom([]);
        $this->assertEquals(StreamHandler::class, get_class($sh));
    }
}
