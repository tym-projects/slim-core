<?php
namespace PHPTDD\src\Factory;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Factory\LoggerHandlerFactory;

class LoggerHandlerFactoryTest extends TestCase
{

    private $settings;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->settings = [
            'filehandler' => [
                'path' => __DIR__,
                'filename' => 'hello.txt',
            ],
            'streamhandler' => [
            ],
            'lokihandler' => [
                'entrypoint' => 'http://loki:3100',
            ],
        ];
    }

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandlerFactory
     * @covers TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler
     * @covers TYM\SlimCore\Factory\LoggerHandler\LokiLoggerHandler
     * @covers TYM\SlimCore\Factory\LoggerHandler\StreamLoggerHandler
     *
     **/
    public function testBuildHandlers()
    {
        $l = LoggerHandlerFactory::buildHandlersFrom($this->settings);
        $this->assertTrue(is_array($l));
    }

    /**
     * @covers TYM\SlimCore\Factory\LoggerHandlerFactory
     * @covers TYM\SlimCore\Factory\LoggerHandler\FileLoggerHandler
     * @covers TYM\SlimCore\Factory\LoggerHandler\LokiLoggerHandler
     * @covers TYM\SlimCore\Factory\LoggerHandler\StreamLoggerHandler
     *
     **/
    public function testBuildHandlersInvalidHandler()
    {
        $this->expectException(InvalidArgumentException::class);
        $l = LoggerHandlerFactory::buildHandlersFrom(['invalidHandler' => []]);

    }

}
