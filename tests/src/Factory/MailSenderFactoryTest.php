<?php
namespace PHPTDD\src\Factory;

use PHPMailer\PHPMailer\PHPMailer;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Factory\MailSenderFactory;

class MailSenderFactoryTest extends TestCase
{

    private $phpmailer;
    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->phpmailer = $this->getMockBuilder(PHPMailer::class)->disableOriginalConstructor()->getMock();
        $this->phpmailer->method('Send')->willReturn(true);
    }

    /**
     * @covers TYM\SlimCore\Factory\MailSenderFactory
     **/
    public function testMailSenderFactory()
    {
        $mailsender = new MailSenderFactory($this->phpmailer);
        $mailsender->configSender([
            "SMTP_PORT" => "25",
            "SMTP_HOST" => "mail.example.com",
            "SMTP_USER" => "user",
            "SMTP_PASS" => "password",
            "FROM_EMAIL" => "info@example.com",
            "FROM_NAME" => "Example User",
        ]);

        $this->assertTrue($mailsender->send(
            ['info@mydbajoaragon.es', 'noreply@mydbajoaragon.es'],
            'hello',
            'Body',
            ['file.txt']
        ));

        $this->assertTrue($mailsender->send(
            ['noreply@mydbajoaragon.es'],
            'hello',
            'Body'
        ));
    }
}
