<?php
namespace PHPTDD\src\Shared;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Shared\Assert;
use TYM\SlimCore\Tests\TestClasses\Foo;

class AssertTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Shared\Assert
     **/
    public function testAssertInvalidArgument()
    {
        $this->expectException(InvalidArgumentException::class);
        Assert::arrayOf(Bar::class, [new Foo()]);
    }

    /**
     * @covers TYM\SlimCore\Shared\Assert
     **/
    public function testAssert()
    {
        $this->assertEquals('', Assert::arrayOf(Foo::class, [new Foo()]));
    }
}
