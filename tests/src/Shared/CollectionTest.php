<?php
namespace PHPTDD\src\Shared;

use ArrayIterator;
use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Tests\TestClasses\Foo;
use TYM\SlimCore\Tests\TestClasses\FooCollection;

class CollectionTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Shared\Collection
     * @covers TYM\SlimCore\Shared\Assert
     **/
    public function testCollection()
    {
        $data = [new Foo()];
        $collection = new FooCollection($data);

        $this->assertEquals(1, $collection->count());
        $this->assertEquals($data, $collection->getItems());
        $this->assertEquals(ArrayIterator::class, get_class($collection->getIterator()));
    }
}
