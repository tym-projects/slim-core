<?php
namespace PHPTDD\src\Shared\ValueObject;

use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Shared\ValueObject\IntValueObject;
use TYM\SlimCore\Tests\TestClasses\IntBar;

class IntValueObjectTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Shared\ValueObject\IntValueObject
     **/
    public function testIntValueObject()
    {
        $i = IntBar::from(20);
        $this->assertEquals(20, $i->value());
        $this->assertFalse(
            $i->isBiggerThan(
                IntBar::from(30)
            )
        );
        $this->assertEquals("20", $i);
    }
}
