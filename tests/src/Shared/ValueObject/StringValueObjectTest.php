<?php
namespace PHPTDD\src\Shared\ValueObject;

use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Tests\TestClasses\StringBar;

class StringValueObjectTest extends TestCase
{

    /**
     * @covers TYM\SlimCore\Shared\ValueObject\StringValueObject
     **/
    public function testStringValueObject()
    {
        $s = StringBar::from('hello');
        $this->assertEquals('hello', $s->value());
        $this->assertEquals('hello', $s);
    }
}
