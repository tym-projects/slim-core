<?php
namespace PHPTDD\src\Utils;

use PHPUnit\Framework\TestCase;
use TYM\SlimCore\Utils\EnvFile;
use TYM\SlimCore\Utils\SecretsLoader;
use TYM\SlimCore\Utils\VInfraFile;

class SecretsLoaderTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers TYM\SlimCore\Utils\SecretsLoader
     * @covers TYM\SlimCore\Utils\VInfraFile
     **/
    public function testVinfraSecretsLoader()
    {
        $s = new SecretsLoader(
            new VInfraFile(),
            __DIR__ . '/../../secrets/vinfra/'
        );

        $s->loadSecrets();

        $this->assertEquals('value1', getenv('secret-1'));
        $this->assertEquals('value2', getenv('secret-2'));
        $this->assertEquals('value1', getenv('name1'));
        $this->assertEquals('value2', getenv('name2'));
    }

    /**
     * @covers TYM\SlimCore\Utils\SecretsLoader
     * @covers TYM\SlimCore\Utils\EnvFile
     **/
    public function testEnvironmentSecretsLoader()
    {
        $s = new SecretsLoader(
            new EnvFile(),
            __DIR__ . "/../../secrets/"
        );

        $s->loadSecrets();

        $this->assertEquals('value', getenv('SECRET'));
        $this->assertEquals('value2', getenv('SECRET2'));
    }
}
